# sherpa-library

sherpa library assessment

## How to run

```
git clone https://gitlab.com/mabrahao/sherpa-library.git
cd sherpa-library
make start
```

## Swagger documentation
```
http://localhost:8089/swagger-ui.html
```

## Technical Roadmap
- Implement authorization to access endpoints
- Improve Client registration
- - Move address to its table
- Improve Book registration
- - Create an Author table
- - Create a Category table

## Product Roadmap
- Implement an inventory table so we can have multiple copies of a book
- Add a feature to organize the library by having sectors and shelves on the application
- Improve the search for books by having better endpoints with more search options and search for different fields, e.g. barcode