package com.sherpa.library.controller;

import com.sherpa.library.entity.Book;
import com.sherpa.library.service.BookService;
import com.sherpa.library.service.BorrowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BookController extends BaseApiController {
    @Autowired
    private BookService bookService;
    @Autowired
    private BorrowService borrowService;

    @PostMapping("/books")
    @ResponseStatus(HttpStatus.CREATED)
    public Book saveBook(@RequestBody Book book) {
        return bookService.save(book);
    }

    @PutMapping("/books")
    public Book updateBook(@RequestBody Book book) {
        return bookService.update(book);
    }

    @GetMapping("/books")
    public List<Book> fetchAll() {
        return bookService.findAll();
    }

    @GetMapping("/books/title/{title}")
    public Book findByTitle(@PathVariable String title) {
        return bookService.findByTitle(title);
    }

    @GetMapping("/books/author/{author}")
    public Book findByAuthor(@PathVariable String author) {
        return bookService.findByAuthor(author);
    }

    @PostMapping("/books/{bookId}/clients/{clientId}/borrow")
    @ResponseStatus(HttpStatus.CREATED)
    public void borrowBook(@PathVariable int bookId, @PathVariable int clientId) {
        borrowService.borrowBook(bookId, clientId);
    }

    @PostMapping("/books/{bookId}/clients/{clientId}/return")
    public void returnBook(@PathVariable int bookId, @PathVariable int clientId) {
        borrowService.returnBook(bookId, clientId);
    }

    @GetMapping("/books/{id}/is-borrowed")
    public boolean isBorrow(@PathVariable int id) {
        return borrowService.isBorrowed(id);
    }
}
