package com.sherpa.library.controller;

import com.sherpa.library.entity.Client;
import com.sherpa.library.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ClientController extends BaseApiController {
    @Autowired
    private ClientService clientService;

    @PostMapping("/clients")
    @ResponseStatus(HttpStatus.CREATED)
    public Client saveClient(@RequestBody Client client) {
        return clientService.save(client);
    }

    @PutMapping("/clients")
    public Client updateClient(@RequestBody Client client) {
        return clientService.update(client);
    }

    @GetMapping("/clients/{id}")
    public Client fetchAll(@PathVariable int id) {
        return clientService.findById(id);
    }

    @GetMapping("/clients")
    public List<Client> fetchAll() {
        return clientService.findAll();
    }
}
