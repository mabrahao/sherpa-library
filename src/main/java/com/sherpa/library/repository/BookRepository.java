package com.sherpa.library.repository;

import com.sherpa.library.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book, Integer> {
    Book findByTitleContaining(String title);
    Book findByAuthorContaining(String author);
}
