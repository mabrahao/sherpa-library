package com.sherpa.library.repository;

import com.sherpa.library.entity.Borrow;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;

public interface BorrowRepository extends JpaRepository<Borrow, Integer> {
    Borrow findByBookIdAndReturned(int id, LocalDateTime returned);
    Borrow findByBookIdAndClientId(int bookId, int clientId);
}
