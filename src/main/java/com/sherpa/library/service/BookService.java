package com.sherpa.library.service;

import com.sherpa.library.entity.Book;
import com.sherpa.library.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {
    @Autowired
    private BookRepository bookRepository;

    public Book save(Book book) {
        return bookRepository.save(book);
    }

    public Book findByTitle(String title) {
        return bookRepository.findByTitleContaining(title);
    }

    public Book findByAuthor(String author) {
        return bookRepository.findByAuthorContaining(author);
    }

    public List<Book> findAll() {
        return bookRepository.findAll();
    }

    public Book update(Book book) {
        Book currentBook = bookRepository.findById(book.getId()).orElse(null);
        if (currentBook == null) {
            throw new RuntimeException("Book Id is missing");
        }
        return bookRepository.save(book);
    }
}
