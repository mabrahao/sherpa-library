package com.sherpa.library.service;

import com.sherpa.library.entity.Book;
import com.sherpa.library.entity.Borrow;
import com.sherpa.library.entity.Client;
import com.sherpa.library.repository.BorrowRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class BorrowService {
    @Autowired
    private BorrowRepository borrowRepository;

    public void borrowBook(int bookId, int clientId) {
        Client client = new Client();
        Book book = new Book();
        client.setId(clientId);
        book.setId(bookId);

        Borrow borrow = new Borrow();
        borrow.setBook(book);
        borrow.setClient(client);

        borrowRepository.save(borrow);
    }

    public void returnBook(int bookId, int clientId) {
        Borrow borrow = borrowRepository.findByBookIdAndClientId(bookId, clientId);
        borrow.setReturned(LocalDateTime.now());
        borrowRepository.save(borrow);
    }

    public boolean isBorrowed(int id) {
        return null != borrowRepository.findByBookIdAndReturned(id, null);
    }
}
