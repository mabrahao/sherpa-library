package com.sherpa.library.service;

import com.sherpa.library.entity.Client;
import com.sherpa.library.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientService {
    @Autowired
    private ClientRepository clientRepository;

    public Client save(Client client) {
        return clientRepository.save(client);
    }

    public Client findById(int id) {
        return clientRepository.findById(id).orElse(null);
    }

    public List<Client> findAll() {
        return clientRepository.findAll();
    }

    public Client update(Client client) {
        Client currentClient = clientRepository.findById(client.getId()).orElse(null);
        if (currentClient == null) {
            throw new RuntimeException("Client Id is missing");
        }
        return clientRepository.save(client);
    }
}
