package com.sherpa.library.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "borrow")
public class Borrow {
    @Id
    @GeneratedValue
    private int id;
    @ManyToOne
    @JoinColumn(name="client_id", nullable = false)
    private Client client;
    @ManyToOne
    @JoinColumn(name="book_id", nullable = false)
    private Book book;
    @CreationTimestamp
    private LocalDateTime date;
    private LocalDateTime returned;
}